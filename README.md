# __Fake iTunes__

## __Overview__

The project is a small prototype of a simple music library UI built in Thymeleaf with the rest of the application exposed as a regular REST API.

## __Description__

The [website](https://fakeitunes-by-vytas.herokuapp.com/v1) currently has:

- a [home page](https://fakeitunes-by-vytas.herokuapp.com/v1) that:
    - upon each call or refresh:
        - lists 5 random _artists_
        - lists 5 random _songs_
        - lists 5 random _genres_
    - has a _search box_ for finding tracks by name

<br>

- a _search result_ page that:
    - gets rendered when a user enters a search term
    - shows all tracks that contain the search term

## __REST endpoints__

The exposure of the REST endpoints can be reviewed [here](https://gitlab.com/v.zarauskas/fakeitunes/-/blob/master/src/main/resources/info/Fake%20iTunes.postman_collection.json)

## __Screen Shots__

The following are few sample screen shots of the two API endpoints rendered.

#### __Home Page__
![Home Page](/src/main/resources/info/Home page.png?raw=true "Home Page")


#### __Search Execution__
![Search Execution](/src/main/resources/info/Search Execution.png?raw=true "Search Execution")
